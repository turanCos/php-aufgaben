<?php


require_once 'funktionen.php';
require_once 'succes.php';

if(isset($_POST['firstname']) & isset($_POST['lastname']) & isset($_POST['email']) & isset($_POST['password']) & isset($_POST['password'])) {

    if (passwortabchecken($_POST['password'], $_POST['password'])) {

        $firstname = filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING);
        $lastname = filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);//Checkt ob es halbegs a normal mail ist
        $passwort = $_POST['password'];


        if (checkName($firstname) === false) {
            echo 'firstname ist nicht korrekt';
        } elseif (checkName($lastname) === false) {
            echo 'lastname ist nicht korrekt';
        } elseif (emailPruefen($email) === false) {
            echo 'email ist nicht korrekt';
        } elseif (kennwortRegex($passwort) === false) {
            echo 'kennwort nicht korrekt bitte um überprüfung';
        } else {

            userAnlegen($firstname, $lastname, $email, $passwort);

        }


    } else {
        echo 'Kennwort stimmt nicht überein ';
    }
}
else
    {
        echo 'Formular unvollständig';
    }

?>