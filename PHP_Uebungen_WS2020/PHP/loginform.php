<?php

if(!empty($_SESSION['errors'])){
    echo '<div class="alert alert-danger" role="alert">';
    foreach ($_SESSION['errors'] as $value){
        //echo '<div class="alert alert-danger" role="alert"> htmlspecialchars($_SESSION[\'errors\']); </div>';
        echo htmlspecialchars($value);
    }
    echo '</div>';

}


?>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<section class="" id="as4">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <form action="PHP/login.php" method="POST">

                    <div class="form-group row">
                        <label for="email" class="col-3 col-form-label">E-mail-Adresse</label>
                        <div class="col-9">
                            <input id="loginemail" name="loginmail" placeholder="peter.parker@gmail.com" type="text"
                                   class="form-control" required="required">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-3 col-form-label">Password</label>
                        <div class="col-9">
                            <input id="loginpassword" name="loginpassword" placeholder="********" type="text"
                                   required="required" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="offset-3 col-9">
                            <button name="login" type="submit" class="btn btn-primary">Einloggen</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</section>

