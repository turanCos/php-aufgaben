<?php

require_once 'register.php';

?>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<form action="#" method="POST">

    <div class="form-group row">
        <label for="firstname" class="col-3 col-form-label">Firstname</label>
        <div class="col-9">
            <input id="firstname" name="firstname" placeholder="Peter" type="text" class="form-control" required="required">
        </div>
    </div>
    <div class="form-group row">
        <label for="lastname" class="col-3 col-form-label">Lastname</label>
        <div class="col-9">
            <input id="lastname" name="lastname" placeholder="Parker" type="text" class="form-control" required="required">
        </div>
    </div>
    <div class="form-group row">
        <label for="email" class="col-3 col-form-label">E-mail-Adresse</label>
        <div class="col-9">
            <input id="email" name="email" placeholder="peter.parker@gmail.com" type="text" class="form-control" required="required">
        </div>
    </div>
    <div class="form-group row">
        <label for="password" class="col-3 col-form-label">Password</label>
        <div class="col-9">
            <input id="password" name="password" placeholder="Kennwort" type="text" required="required" class="form-control">
        </div>
    </div>
    <div class="form-group row">
        <label for="password" class="col-3 col-form-label">Password Bestätigen</label>
        <div class="col-9">
            <input id="password" name="password" placeholder="Kennwort Wiederholen" type="text" class="form-control" required="required">
        </div>
    </div>
    <div class="form-group row">
        <div class="offset-3 col-9">
            <button name="submit" type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>