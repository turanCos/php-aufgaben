<?php


    // HTML-Ausgabe eine unsortierte Liste mit den Inhalten
    //Superglobale Variable $_SE
function printServerDetails(){
    //var_dump($_SERVER);
    echo '<ul>';
    foreach ($_SERVER as $key => $value){
        echo '<li>';
        echo $key.': '.$value;
        echo '</li>';
    }
    echo '<ul>';
}


  //Die Funktion überprüft welchen Browser der Nutzer verwendet und gibt ein Feedback welchen der Nutzer benutzt
function browserDetails(){
    //echo $_SERVER['HTTP_USER_AGENT'];
    if($_SERVER['HTTP_USER_AGENT'] === 'Mozilla (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0'){
        echo ' Mozilla ist dein browser';
    }
    elseif ($_SERVER['HTTP_USER_AGENT'] === 'Chrome (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36'){
        echo 'Chrome ist dein Browser';
    }
    else{
        echo 'Nutzung von einem anderen Browser';
    }
}


 // Die Funktion überprüft die Syntax einer Email adresse und gibt true oder false zurück
 // @return bool Je nachdem ob die Email korrekt war wird true oder false zurück gegeben
function emailPruefen($email){
    $s = '/^[A-Z0-9._-]+@[A-Z0-9][A-Z0-9.-]{0,61}[A-Z0-9]\.[A-Z.]{2,6}$/i';
    if(preg_match($s, $email))
    {
        return true;
    }
    else {
        return false;
    }
}


 // Trimmt den übergebenen Parameter und teilt ihn in ein Array. Anschließend ruft die Methode eine weitere
 //Methode (test())auf welche überprüft ob der Parameter den Kriterien entspricht. Diese Methode verändert jedoch nicht
 //den ursprünglichen Parameter. (BIS AUF DAS TRIM()!)

function checkName($name){
    $count = 1;
    $name = trim($name);
    //$ganzerName = explode(' ', $name);

   // foreach ($ganzerName as $item) {
       // if(test($item) === true){
    if(test ($name) ===true){
           // echo 'Name Nr. '.$count.' ist ok';
            $count ++;
            //echo("<br />");
            return $name;
        }
        else {
            echo 'Name Nr. '.$count.' ist NICHT ok';
            $count ++;
           //echo("<br />");
            return false;
        }

}


 // Überprüft ob der Übergebene Name den Kriterien entspricht
 //@param $check zu überprüfender Wert
 //@return bool gibt zurück ob der Wert den Kriterien entspricht oder nicht

function test($check){
    if(preg_match('/^[\p{L} ]+$/u', $check)){
        return true;
    }
    else {
        return false;
    }
}


 // Call by Reference. Der übergebene Parameter ($name2) wird zuerst getrimmt und in ein Array geteilt.
 //Anschließend ruft die Methode eine weitere Methode (test())auf welche überprüft ob der Parameter den Kriterien entspricht.
 //Besonders ist, dass der übergebene Parameter innerhalb der Methode verändert und anschließend zurückgegeben wird.
 //@param $name2 enthält den zu überprüfenden Namen
 //@return string gibt den veränderten Namen zurück

function checkName2(&$name2){
    $count = 1;
    $name2 = trim($name2);
    $ganzerName = explode(' ', $name2);

    foreach ($ganzerName as $item) {
        if(test($item) === true){
            echo 'Name Nr. '.$count.' ist ok';
            $count ++;
            echo("<br />");
        }
        else {
            echo 'Name Nr. '.$count.' ist NICHT ok';
            $count ++;
            echo("<br />");
        }
    }
    return $name2;
}


 // Sortiert die Übergebnen Namen Wahlweise austeigend oder absteigend nach dem Alphabet
function sortiereNamen($namesliste, $sortierart){
    if($sortierart){
        sort($namesliste);
    }
    else {
        rsort($namesliste);
    }
    echo '<ul>';
    foreach ($namesliste as $key => $val) {
        echo '<li>';
        echo $val .("<br />");
        echo '</li>';
    }
    echo '</ul>';

}


 //Errechnet das anhand des Parmeters übergebenen Wertes das aktuelle Alter aus
 // @param $geb_str das Geburtsdatum anhand dessen das aktuelle Alter errechnet werden kann

function errechnealter($geb_str) {
    $tag=substr($geb_str,0,2);
    $mon=substr($geb_str,2,2);
    $jahr=substr($geb_str,4,4);
    if(is_numeric($tag)&& is_numeric($mon)&& is_numeric($jahr)){
        $heute=time();
        //Geburtstag im aktuellen Jahr
        $geb_aktuell=mktime(0,0,0,$mon,$tag,date("Y",$heute));

        //Heuriges Jahr - geburtsjahr
        $alter=date("Y",$heute)-$jahr;

        //Wenn Geburtstag dieses Jahr noch nicht stattfand wird 1 Jahr subtrahiert
        if ($heute<=$geb_aktuell) $alter--;
        echo 'Du bist '.$alter.' Jahre alt';
    }
    else{
        echo'Die Eingabe ist nicht korrekt.';
        return;
    }


    function passwortabchecken($passwort1, $passwort2)
    {
        if($passwort1 === $passwort2)
        {
            return true;
        }
        else
            {
                return false;
            }
    }



    function kennwortRegex($passwort)
    {
        $s = '/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/';

        if(preg_match($s, $passwort))
        {
            return true;
        }else
            {
                return false;
            }



    }











}